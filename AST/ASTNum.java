package AST;

import Env.IEnvironment;
import compiler.CodeBlock;
import compiler.CompilerEnvironment;

public class ASTNum implements ASTNode {
	
	int val;

	public ASTNum(int val) {
		this.val = val;
	}
	@Override
	public int eval(IEnvironment env) {
		return val;
	}
	@Override
	public void compile(CompilerEnvironment env, CodeBlock code) {
		code.num(val);;
	}

}
