package compiler;
public class Address {
	private int level;
	private int offset;
	
	public Address(int level, int offset) {
		this.level = level;
		this.offset = offset;
	}
	
	public int getLevel() { //representa  o nr de vezes que vamos ter que subir nos pais
		return level;
	}
	
	public int getOffset() { //campo dentro da frame (bloco/ excluindo o SL)
		return offset;
	}

}
