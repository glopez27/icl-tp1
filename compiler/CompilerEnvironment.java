package compiler;
import java.util.HashMap;
import java.util.Map;

public class CompilerEnvironment {
	Map<String, Integer> map; //nome da variavel, offset
	CompilerEnvironment parent; 
	
	public CompilerEnvironment(CompilerEnvironment parent) {
		this.parent = parent;
		map = new HashMap<String, Integer>();
	}
	
	public CompilerEnvironment beginScope() {
		CompilerEnvironment env = new CompilerEnvironment(this);
		return env;
	}

	public CompilerEnvironment endScope() {
		return parent;
	}

	public void addAssoc(String id, int value) {
		map.put(id, value);
	}
	
	public Address find(String id, int nLevels) { //chamar a funcao no ambiente de compiler a zero 
		if(map.containsKey(id)) {
			return new Address(nLevels, map.get(id));
		}
		return parent.find(id, nLevels+1);
	}
	

}
