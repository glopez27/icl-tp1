package compiler;
import java.io.PrintStream;

public class StackFrame {
	public final static String VARIABLE = "loc_";
	public final static String SL_CONST = "SL"; 
	
	int id; //f(id) � o nr da frame
	StackFrame SL; //PAI
	int nr_var; //o nr de campos


	public StackFrame(StackFrame SL,int id) {
		this.id = id;
		this.SL = SL;
		nr_var = 0;
	}
	
	public int getId() {
		return id;
	}
	
	public StackFrame getSL() {
		return SL;
	}
	

	public void addVariable(){ //tantos addVar como nr de var
		nr_var++;
	}
	
	public int getNrVar() {
		return nr_var;
	}

	
    /* 
     * frame_id.j 
     *  
			.class frame_id 
			.super java/lang/Object 
			.field public SL Lancestor_frame_id;  
			.field public loc_00 type; 
			.field public loc_01 type; 
			.. 
			.field public loc_n type; 
			.end method  
     */ 
	public void dump(PrintStream out) {
		out.println(".class frame" + id);
    	out.println(".super java/lang/Object");
    	if(SL != null)
    		out.println(".field public sl Lframe" + SL.getId() + ";");
    	else
    		out.println(".field public sl Ljava/lang/Object;");
  		
    	for(int i=0; i < nr_var; i++)
    		out.println(".field public x" + i + " I");
    	out.println("\n.method public <init>()V");
    	out.println("	aload_0");
    	out.println("	invokenonvirtual java/lang/Object/<init>()V");
    	out.println("	return");
    	out.println(".end method");
    } 
	


}
