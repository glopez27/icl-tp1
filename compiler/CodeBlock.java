package compiler;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Set;
import java.util.Map.Entry;

import AST.ASTNode;


public class CodeBlock {
	
	ArrayList<String> code; //msg
	ArrayList<StackFrame> frames; //todas as frames necessarias (nr de let's)
	CompilerEnvironment environment;
	StackFrame current; //onde se esta no momento
	
	public CodeBlock() {
		current = null;
		code = new ArrayList<String>();
		frames = new ArrayList<StackFrame>();
		environment = new CompilerEnvironment(null);
	}
	
	public void div() {
		code.add("idiv");
	}
	
	public void num(int n) {
		code.add("sipush " + n);
	}

	public void add() {
		code.add("iadd");
	}
	
	public void sub() {
		code.add("isub");
	}
	
	public void mul() {
		code.add("imul");
	}
	
	public void dup() {
		code.add("dup");
	}
	
	public void id(Address a) { 
		StackFrame temp = current;
		
		code.add("aload 4");
		for(int i = 0; i < a.getLevel(); i++){
			code.add("getfield frame"  + temp.getId() + "/sl Lframe" + temp.getSL().getId() + ";"); 
			temp = temp.getSL();
		}
		code.add("getfield frame"  + temp.getId() + "/x" + a.getOffset() + " I"); 
	}
	
	public void let(Set<Entry<String, ASTNode>> set, CompilerEnvironment env) {
		for(Entry<String, ASTNode> e: set) { //para cada variavel
			code.add("aload 4"); 
			e.getValue().compile(env, this); //cria o codigo para a expressao e no final deixa o valor da var no topo da ES
			//env do compilador
			env.addAssoc(e.getKey(), current.getNrVar()); //offset � o current.getNrVar() pq estamos na frame correta
			code.add("putfield frame"  + current.getId() + "/x" + current.getNrVar() + " I");
			current.addVariable();
		}
	}
	
	public void createFrame() {
		StackFrame frame = new StackFrame(current, frames.size()); 
		code.add("new frame" + frame.getId() );
		code.add("dup");
		code.add("invokespecial frame" + frame.getId() + "/<init>()V");
		code.add("dup");
		code.add("aload 4"); //busca o endere�o da current frame
		if(current == null)
			code.add("putfield frame"  + frame.getId() + "/sl Ljava/lang/Object;");
		else
			code.add("putfield frame"  + frame.getId() + "/sl Lframe" + current.getId() + ";"); //pega no primeiro campo da ES e vai po-lo na posicao que nos dissermos do outro compo
		code.add("astore 4");
		current = frame;
		frames.add(frame);
	}
	
	public void deleteFrame() {
		code.add("aload 4"); 
		if(current.getId() == 0)
			code.add("getfield frame"  + current.getId() + "/sl Ljava/lang/Object;");
		else
			code.add("getfield frame"  + current.getId() + "/sl Lframe" + current.getSL().getId() + ";"); //exemplo se tivermo 2 lets dentro de 1 let, o pai do segundo 2 vai ser do tipo 0	
		code.add("astore 4");
		current = current.getSL();
	}
		
	
	//escrever codigo principal
	public String dumpCode() {
		
		String res="";
		for(String s : code) {
			res+= ("       "+s+"\n");
		}
		return res;
	}
	
	//escrever os pequenos codigos das frames
	void dumpFrames() throws FileNotFoundException { 
		for( StackFrame frame: frames) {
			PrintStream out = new PrintStream(new FileOutputStream("frame" + frame.getId()+".j"));
			frame.dump(out);
		}
	} 
	

}
