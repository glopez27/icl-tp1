package compiler;
import java.io.FileNotFoundException;

public interface ICompiler {

	public void compile() throws FileNotFoundException;
}
