package AST;

import Env.IEnvironment;
import compiler.Address;
import compiler.CodeBlock;
import compiler.CompilerEnvironment;

public class ASTId implements ASTNode{
	String id;
	
	public ASTId(String id){
		this.id = id;
	}

	@Override
	public int eval(IEnvironment env) {
		return env.find(id);
	}

	@Override
	public void compile(CompilerEnvironment env, CodeBlock code) {
		Address a = env.find(id, 0);
		code.id(a);
	}

}
