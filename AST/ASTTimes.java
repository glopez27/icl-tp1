package AST;

import Env.IEnvironment;
import compiler.CodeBlock;
import compiler.CompilerEnvironment;

public class ASTTimes implements ASTNode {
	
	ASTNode lN;
	ASTNode rN;

	public ASTTimes(ASTNode n1, ASTNode n2) {
		lN = n1;
		rN = n2;
	}
	@Override
	public int eval(IEnvironment env) {
		int aux1 = lN.eval(env);
		int aux2 = rN.eval(env);
		
		return aux1*aux2;
	}
	@Override
	public void compile(CompilerEnvironment env, CodeBlock code) {
		lN.compile(env, code);
		rN.compile(env, code);
		code.mul();
	}

}
