package AST;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import Env.IEnvironment;
import compiler.CodeBlock;
import compiler.CompilerEnvironment;

public class ASTLet implements ASTNode{
	Map<String, ASTNode> mapAux;
	ASTNode body;
	
	public ASTLet(Map<String, ASTNode> map, ASTNode body){
		mapAux= map;
		this.body = body;
	}

	@Override
	public int eval(IEnvironment env) {
		IEnvironment newEnv = env.beginScope();
		
		Set<Entry<String, ASTNode>> setOfMap = mapAux.entrySet();
		
		for(Entry<String, ASTNode> x: setOfMap) { 
			int v1 = x.getValue().eval(env); 
			newEnv.addAssoc(x.getKey(), v1);
		}
		
		int v2 = body.eval(newEnv);
		env= newEnv.endScope();
		return v2;
	}

	@Override
	public void compile(CompilerEnvironment env, CodeBlock code) {
		Set<Entry<String, ASTNode>> setOfMap = mapAux.entrySet();
		CompilerEnvironment newCompEnv = env.beginScope();
		code.createFrame(); 
		code.let(setOfMap, newCompEnv);
		body.compile(newCompEnv, code);
		code.deleteFrame();
	}


}
