package AST;
import Env.IEnvironment;
import compiler.CodeBlock;
import compiler.CompilerEnvironment;

public interface ASTNode {
	int eval(IEnvironment env);
	
	void compile(CompilerEnvironment env, CodeBlock code);

}
