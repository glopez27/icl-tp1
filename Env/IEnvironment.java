package Env;
public interface IEnvironment {
	IEnvironment beginScope();
	IEnvironment endScope();
	void addAssoc(String id, int value);
	int find(String id);

}
