package Env;

import java.util.HashMap;
import java.util.Map;

public class Environment implements IEnvironment{
	Environment parent;
	Map<String, Integer> map;
	
	public Environment(Environment parent){
		this.parent = parent;
		this.map = new HashMap<String, Integer>();
	}

	@Override
	public IEnvironment beginScope() {
		Environment env = new Environment(this);
		return env;
	}

	@Override
	public IEnvironment endScope() {
		return parent;
	}

	@Override
	public void addAssoc(String id, int value) {
		map.put(id, value);
	}

	@Override
	public int find(String id) {
		if(map.containsKey(id)) {
			return map.get(id);
		}
		return parent.find(id);
	}

}
